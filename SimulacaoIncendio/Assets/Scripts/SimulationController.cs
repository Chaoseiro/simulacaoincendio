﻿using UnityEngine;

public class SimulationController : MonoBehaviour
{
    string[] m_texts =
    {
        "Utilize as teclas W, A, S e D para se movimentar, Shift Esquerdo para se agachar, o mouse para controlar a visão da câmera e o E para interagir com objetos.",
        "O simulador mostrará como a pessoa deve se comportar em cada uma das possíveis situações durante um incêndio. Preste muita atenção.",
        "Mantenha-se abaixado para não respirar a fumaça",
        "Tenha cuidado por onde anda, preste atenção ao seu redor para não se acidentar",
        "Não utilize o elevador"
    };

    int m_currentRoom = 0;
    int m_currentText = 0;

    [SerializeField]
    HudController m_hudController;

    [SerializeField]
    PlayerController m_player;

    [SerializeField]
    float m_introTextTime = 10;

    float m_timer = 0;
    bool m_finishedIntroText = false;

    [SerializeField]
    DoorTriggerController m_firstDoor;

    [SerializeField]
    GameObject[] m_lockers;

    // Start is called before the first frame update
    void Start()
    {
        m_firstDoor.Locked = true;

        m_finishedIntroText = false;

        for (int i = 0; i < m_lockers.Length; i++)
        {
            m_lockers[i].SetActive(false);
        }

        m_hudController.FadeDown(OnFadeDownFinish);
    }

    void OnFadeDownFinish()
    {
        m_currentText = 0;
        m_hudController.SetText(m_texts[m_currentText]);
    }

    float m_deltaTime = 0;
    // Update is called once per frame
    void Update()
    {
        m_deltaTime = Time.deltaTime;

        if(!m_finishedIntroText)
        {
            m_timer += m_deltaTime;

            if(m_timer >= m_introTextTime)
            {
                m_currentText++;
                m_hudController.SetText(m_texts[m_currentText]);
                m_finishedIntroText = true;

                m_firstDoor.Locked = false;
            }
        }

        if(m_currentRoom == 1)
        {
            if (m_player.IsCrouching)
                m_hudController.DeactivateWarning();
            else
                m_hudController.ActivateWarning();
        }
    }

    public void EnteredRoom(int room)
    {
        if (room < m_currentRoom)
            return;
        
        m_currentRoom = room;
        for (int i = 0; i < m_currentRoom; i++)
        {
            m_lockers[i].SetActive(true);
        }
    }

    public void OnDoorOpened()
    {
        m_currentText++;
        m_hudController.SetText(m_texts[m_currentText]);
    }

    public void HideText()
    {
        m_hudController.HideText();
    }

    public void EnteredElevator()
    {
        m_currentText = m_texts.Length - 1;
        m_hudController.SetText(m_texts[m_currentText]);
        m_hudController.ActivateWarning();
    }

    public void ExitElevator()
    {
        m_hudController.DeactivateWarning();
        m_hudController.HideText();
    }

    public void StaircaseDoorOpened()
    {
        m_hudController.FadeUp(OnFadeFinish);
    }

    void OnFadeFinish()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
