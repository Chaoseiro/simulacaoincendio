﻿using UnityEngine;

public class DoorTriggerController : MonoBehaviour
{
    [SerializeField]
    Animator m_doorAnimator;

    [SerializeField]
    string m_openDoorBool = "Open";

    bool m_doorOpen = false;

    public bool Locked = false;

    public UnityEngine.Events.UnityEvent OnDoorOpened;

    private void Start()
    {
        m_doorOpen = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (m_doorOpen || Locked)
            return;

        if(Input.GetButtonDown("Interact"))
        {
            m_doorOpen = true;
            if(m_doorAnimator)
                m_doorAnimator.SetBool(m_openDoorBool, true);

            OnDoorOpened?.Invoke();
        }
    }
}
