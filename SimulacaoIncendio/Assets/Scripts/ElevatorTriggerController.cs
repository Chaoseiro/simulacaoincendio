﻿using UnityEngine;

public class ElevatorTriggerController : MonoBehaviour
{
    [SerializeField]
    SimulationController m_controller;

    private void OnTriggerEnter(Collider other)
    {
        // Gave Over
        Debug.Log("Entered Elevator!");
        m_controller.EnteredElevator();
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Exit from Elevator");
        m_controller.ExitElevator();
    }
}
