﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardTriggerController : MonoBehaviour
{
    [SerializeField]
    HudController m_hudController;

    private void OnTriggerStay(Collider other)
    {
        if(other != null && other.CompareTag("Player"))
            m_hudController.TakeHit();
    }
}
