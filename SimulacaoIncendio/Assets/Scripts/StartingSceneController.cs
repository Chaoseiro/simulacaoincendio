﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingSceneController : MonoBehaviour
{
    private void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }

    public void OnStart()
    {
        Cursor.visible = false;
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    public void OnExit()
    {
        Application.Quit();
    }
}
