﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HudController : MonoBehaviour
{
    [SerializeField]
    CanvasGroup m_textCanvasGroup;
    [SerializeField]
    [Range(0, 1)]
    float m_maxTextAlpha = 0.8f;

    [SerializeField]
    float m_changeTextTime = 1;

    [SerializeField]
    TextMeshProUGUI m_text;

    //[SerializeField]
    bool m_changingText = false;
    bool m_hidingText = false;
    float m_textAlpha = 0;
    float m_textTimer = 0;
    string m_nextText = string.Empty;
    bool m_fadingUpText = false;

    [SerializeField]
    Image m_warningEffectImage;
    [SerializeField]
    [Range(0, 1)]
    float m_minWarningEffectAlpha = 0.6f;
    [SerializeField]
    [Range(0, 1)]
    float m_maxWarningEffectAlpha = 0.9f;
    [SerializeField]
    float m_warningEffectSpeed = 5;
    [SerializeField]
    float m_takingHitMultiplier = 5;

    Color m_warningColor;
    float m_warningAlpha = 0;
    bool m_fadingUpWarning = false;
    //[SerializeField]
    bool m_warningEffectActive = false;
    //[SerializeField]
    bool m_takingHit = false;

    [SerializeField]
    Image m_faderImage;
    [SerializeField]
    float m_fadeTime = 1;
    Color m_fadeColor;
    float m_faldeAlpha = 0;
    float m_fadeTimer = 0;
    bool m_fading = false;
    bool m_fadingUp = false;

    System.Action m_onFadeFinish;

    // Start is called before the first frame update
    void Start()
    {
        m_changingText = false;
        m_hidingText = false;
        m_warningEffectActive = false;
        m_textAlpha = 0;
        m_textTimer = 0;

        m_fadingUpText = false;
        m_fadingUpWarning = false;

        m_textCanvasGroup.alpha = m_textAlpha;

        m_warningColor = m_warningEffectImage.color;
        m_warningAlpha = 0;
        m_warningColor.a = m_warningAlpha;
        m_warningEffectImage.color = m_warningColor;
    }

    float m_deltaTime;
    // Update is called once per frame
    void Update()
    {
        m_deltaTime = Time.deltaTime;
        if (m_warningEffectActive)
        {
            if(m_fadingUpWarning)
            {
                m_warningAlpha += m_warningEffectSpeed * m_deltaTime * (m_takingHit ? m_takingHitMultiplier : 1);
                if (m_warningAlpha >= m_maxWarningEffectAlpha)
                    m_fadingUpWarning = false;
            }
            else
            {
                m_warningAlpha -= m_warningEffectSpeed * m_deltaTime * (m_takingHit ? m_takingHitMultiplier : 1);
                if (m_warningAlpha <= m_minWarningEffectAlpha)
                {
                    if (m_takingHit)
                    {
                        m_takingHit = false;
                        DeactivateWarning();
                    }
                    else
                    {
                        m_fadingUpWarning = true;
                    }
                }
            }
        }
        else
        {
            if(m_warningAlpha > 0)
            {
                m_warningAlpha -= m_warningEffectSpeed * m_deltaTime;
            }
        }

        m_warningColor.a = m_warningAlpha;
        m_warningEffectImage.color = m_warningColor;

        if(m_changingText)
        {
            m_textTimer += m_deltaTime;
            if (m_fadingUpText)
            {
                m_textAlpha = Mathf.Lerp(0, m_maxTextAlpha, m_textTimer / (m_changeTextTime / 2.0f));
                if (m_textTimer >= m_changeTextTime / 2.0f)
                {
                    m_changingText = false;
                    m_fadingUpText = false;
                    m_textTimer = 0;
                }
            }
            else
            {
                if(m_textAlpha > 0)
                    m_textAlpha = Mathf.Lerp(m_maxTextAlpha, 0, m_textTimer / (m_changeTextTime / 2.0f));
                if (m_textTimer >= m_changeTextTime / 2.0f)
                {
                    m_textTimer = 0;
                    m_text.text = m_nextText;
                    m_fadingUpText = true;
                }
            }
        }

        if (m_hidingText)
        {
            if (m_textAlpha > 0)
                m_textAlpha = Mathf.Lerp(m_maxTextAlpha, 0, m_textTimer / (m_changeTextTime / 2.0f));
            if (m_textTimer >= m_changeTextTime / 2.0f)
            {
                m_textTimer = 0;
                m_hidingText = false;
                m_fadingUpText = true;
            }
        }

        m_textCanvasGroup.alpha = m_textAlpha;

        if (m_fading)
        {
            m_fadeTimer += m_deltaTime;
            
            if (m_fadingUp)
            {
                m_faldeAlpha = Mathf.Lerp(0, 1, m_fadeTimer / m_fadeTime);
            }
            else
            {
                m_faldeAlpha = Mathf.Lerp(1, 0, m_fadeTimer / m_fadeTime);
            }

            m_fadeColor.a = m_faldeAlpha;
            m_faderImage.color = m_fadeColor;

            if (m_fadeTimer >= m_fadeTime)
            {
                m_fadeTimer = 0;
                m_fading = false;
                m_onFadeFinish?.Invoke();
            }
        }
    }

    public void ActivateWarning()
    {
        if (m_warningEffectActive)
            return;
        m_warningEffectActive = true;
        m_fadingUpWarning = true;
    }

    public void DeactivateWarning()
    {
        m_warningEffectActive = false;
    }

    public void TakeHit()
    {
        if (m_takingHit)
            return;

        m_takingHit = true;
        ActivateWarning();
    }

    public void SetText(string newText)
    {
        m_textTimer = 0;
        m_nextText = newText;
        m_changingText = true;
    }

    public void HideText()
    {
        m_textTimer = 0;
        m_changingText = false;
        m_hidingText = true;
        m_fadingUpText = false;
    }

    public void FadeUp(System.Action onFadeFinish = null)
    {
        m_onFadeFinish = onFadeFinish;

        m_fading = true;
        m_fadingUp = true;
        m_fadeTimer = 0;
    }
    public void FadeDown(System.Action onFadeFinish = null)
    {
        m_onFadeFinish = onFadeFinish;

        m_fading = true;
        m_fadingUp = false;
        m_fadeTimer = 0;
    }
}
