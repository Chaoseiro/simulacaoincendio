﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTriggerController : MonoBehaviour
{
    [SerializeField]
    int m_roomNumber = 0;

    [SerializeField]
    SimulationController m_controller;

    private void OnTriggerEnter(Collider other)
    {
        if (other != null && other.CompareTag("Player"))
        {
            if (m_controller)
            {
                m_controller.EnteredRoom(m_roomNumber);
            }
        }
    }
}
